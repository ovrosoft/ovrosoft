#pragma checksum "D:\SUMON\Ovrosoft\WebApp\Views\WebSite\Home.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "31c96b8784e9ee4afd12f46cea14dddb0deb7f81"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_WebSite_Home), @"mvc.1.0.view", @"/Views/WebSite/Home.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"31c96b8784e9ee4afd12f46cea14dddb0deb7f81", @"/Views/WebSite/Home.cshtml")]
    public class Views_WebSite_Home : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\SUMON\Ovrosoft\WebApp\Views\WebSite\Home.cshtml"
  
    ViewData["Title"] = "Home";
    Layout = "~/Views/Shared/Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<!--pos solution-->
<div class=""container products_pack"">
    <div class=""row"">
        <div class=""col-sm-12 products-head"">
            <h2>Dynamic E-Commerce Solution</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class=""col-sm-12"">
            <div class=""row product-box"">
                <div class=""col-sm-4""><img src=""/images/products/apple.jpg"" alt=""accounting solution"" /></div>
                <div class=""col-sm-8"">
                    <div class=""row"">
                        <div class=""col-sm-12 feature-list"">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>");
            WriteLiteral(@"
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class=""col-sm-12"">
                            <p>Ovrosoft Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>                            <div class=""product-more"">
                                <div class=""pull-le");
            WriteLiteral(@"ft share-btn"">
                                    <a class=""facebook"" href=""#""><i class=""fa fa-facebook""></i> Share</a>
                                    <a class=""gplus"" href=""#""><i class=""fa fa-google-plus""></i> Share</a>
                                    <a class=""twitter"" href=""#""><i class=""fa fa-twitter""></i> Tweet</a>
                                    <a class=""linkedin"" href=""#""><i class=""fa fa-linkedin""></i> LinkedIn</a>
                                </div>
                                <div class=""pull-right"">
                                    <a href=""/website/ecommerce"" class=""btn btn-primary btn-sm""> <span class=""glyphicon glyphicon-link""></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--pos solution-->
<div class=""container products_pack"">
    <div class=""row"">
        <div class");
            WriteLiteral(@"=""col-sm-12 products-head"">
            <h2>Corporate Company Website</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class=""col-sm-12"">
            <div class=""row product-box"">
                <div class=""col-sm-8"">
                    <div class=""row"">
                        <div class=""col-sm-12 feature-list"">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class=""");
            WriteLiteral(@"col-sm-12"">
                            <p>Ovrosoft Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>                            <div class=""product-more"">
                                <div class=""pull-left share-btn"">
                                    <a class=""facebook"" href=""#""><i class=""fa fa-facebook""></i> Share</a>
                                    <a class=""gplus"" href=""#""><i class=""fa fa-google-plus""></i");
            WriteLiteral(@"> Share</a>
                                    <a class=""twitter"" href=""#""><i class=""fa fa-twitter""></i> Tweet</a>
                                    <a class=""linkedin"" href=""#""><i class=""fa fa-linkedin""></i> LinkedIn</a>
                                </div>
                                <div class=""pull-right"">
                                    <a href=""/website/Company"" class=""btn btn-primary btn-sm""> <span class=""glyphicon glyphicon-link""></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""col-sm-4""><img src=""/images/products/apple.jpg"" alt=""accounting solution"" /></div>
            </div>
        </div>
    </div>
</div>


<!--pos solution-->
<div class=""container products_pack"">
    <div class=""row"">
        <div class=""col-sm-12 products-head"">
            <h2>Education School/College Website</h2>
            <p>Import E");
            WriteLiteral(@"xport Easy Solution</p>
        </div>
        <div class=""col-sm-12"">
            <div class=""row product-box"">
                <div class=""col-sm-4""><img src=""/images/products/apple.jpg"" alt=""accounting solution"" /></div>
                <div class=""col-sm-8"">
                    <div class=""row"">
                        <div class=""col-sm-12 feature-list"">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        ");
            WriteLiteral(@"<div class=""col-sm-12"">
                            <p>Ovrosoft Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>                            <div class=""product-more"">
                                <div class=""pull-left share-btn"">
                                    <a class=""facebook"" href=""#""><i class=""fa fa-facebook""></i> Share</a>
                                    <a class=""gplus"" href=""#""><i class=""fa fa-goog");
            WriteLiteral(@"le-plus""></i> Share</a>
                                    <a class=""twitter"" href=""#""><i class=""fa fa-twitter""></i> Tweet</a>
                                    <a class=""linkedin"" href=""#""><i class=""fa fa-linkedin""></i> LinkedIn</a>
                                </div>
                                <div class=""pull-right"">
                                    <a href=""/website/education"" class=""btn btn-primary btn-sm""> <span class=""glyphicon glyphicon-link""></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--pos solution-->
<div class=""container products_pack"">
    <div class=""row"">
        <div class=""col-sm-12 products-head"">
            <h2>News, Magazine and Advertisement Solution</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class=""col-sm-12"">
            <di");
            WriteLiteral(@"v class=""row product-box"">
                <div class=""col-sm-8"">
                    <div class=""row"">
                        <div class=""col-sm-12 feature-list"">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class=""col-sm-12"">
                            <p>Ovrosoft Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinar");
            WriteLiteral(@"y PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>                            <div class=""product-more"">
                                <div class=""pull-left share-btn"">
                                    <a class=""facebook"" href=""#""><i class=""fa fa-facebook""></i> Share</a>
                                    <a class=""gplus"" href=""#""><i class=""fa fa-google-plus""></i> Share</a>
                                    <a class=""twitter"" href=""#""><i class=""fa fa-twitter""></i> Tweet</a>
                                    <a class=""linkedin"" href=""#""><i cla");
            WriteLiteral(@"ss=""fa fa-linkedin""></i> LinkedIn</a>
                                </div>
                                <div class=""pull-right"">
                                    <a href=""/website/news"" class=""btn btn-primary btn-sm""> <span class=""glyphicon glyphicon-link""></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=""col-sm-4""><img src=""/images/products/apple.jpg"" alt=""accounting solution"" /></div>
            </div>
        </div>
    </div>
</div>

<!--pos solution-->
<div class=""container products_pack"">
    <div class=""row"">
        <div class=""col-sm-12 products-head"">
            <h2>Personal Portfolio</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class=""col-sm-12"">
            <div class=""row product-box"">
                <div class=""col-sm-4""><img src=""/images/products/apple.jpg"" alt=""accounting");
            WriteLiteral(@" solution"" /></div>
                <div class=""col-sm-8"">
                    <div class=""row"">
                        <div class=""col-sm-12 feature-list"">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class=""col-sm-12"">
                            <p>Ovrosoft Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC wi");
            WriteLiteral(@"th a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>
                            <div class=""product-more"">
                                <div class=""pull-left share-btn"">
                                    <a class=""facebook"" href=""#""><i class=""fa fa-facebook""></i> Share</a>
                                    <a class=""gplus"" href=""#""><i class=""fa fa-google-plus""></i> Share</a>
                                    <a class=""twitter"" href=""#""><i class=""fa fa-twitter""></i> Tweet</a>
                                    <a class=""linkedin"" href=""#""><i class=""f");
            WriteLiteral(@"a fa-linkedin""></i> LinkedIn</a>
                                </div>
                                <div class=""pull-right"">
                                    <a href=""/website/portfolio"" class=""btn btn-primary btn-sm""> <span class=""glyphicon glyphicon-link""></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
