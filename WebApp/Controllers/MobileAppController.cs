﻿using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    [Route("mobile-app")]
    public class MobileAppsController : Controller
    {
        [Route("mobile-apps")]
        public IActionResult Home()
        {
            return View();
        }
    }
}
