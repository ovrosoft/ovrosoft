﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class SoftwareController : Controller
    {
        [Route("software")]
        public IActionResult Home()
        {
            return View();
        }

        [Route("software/erp-software")]
        [Route("software/erp-solution")]
        public IActionResult Erp()
        {
            return View();
        }

        [Route("software/accounting-solution")]
        [Route("software/accounting-software")]
        public IActionResult Accounting()
        {
            return View();
        }

        [Route("software/real-estate-management")]
        public IActionResult RealEstate()
        {
            return View();
        }

        [Route("software/school-management-system")]
        public IActionResult Sms()
        {
            return View();
        }

        [Route("software/human-resource-management")]
        public IActionResult Hrm()
        {
            return View();
        }

        [Route("software/pos")]
        [Route("software/point-of-sales")]
        public IActionResult Pos()
        {
            return View();
        }

        [Route("software/pharmacy-management-system")]
        public IActionResult Pharmacy()
        {
            return View();
        }
    }
}
