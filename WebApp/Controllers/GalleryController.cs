﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class GalleryController : Controller
    {
        [Route("gallery")]
        public IActionResult Gallery()
        {
            return View();
        }
    }
}
