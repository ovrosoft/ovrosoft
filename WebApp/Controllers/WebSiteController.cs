﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class WebSiteController : Controller
    {
        [Route("website")]
        public IActionResult Home()
        {
            return View();
        }

        [Route("website/ecommerce")]
        public IActionResult Ecommerce()
        {
            return View();
        }

        [Route("website/company")]
        public IActionResult Company()
        {
            return View();
        }

        [Route("website/education")]
        public IActionResult Education()
        {
            return View();
        }

        [Route("website/news")]
        public IActionResult News()
        {
            return View();
        }

        [Route("website/portfolio")]
        public IActionResult Portfolio()
        {
            return View();
        }
    }
}
