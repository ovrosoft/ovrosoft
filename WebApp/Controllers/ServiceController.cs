﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class ServiceController : Controller
    {
        [Route("service")]
        public IActionResult Home()
        {
            return View();
        }
    }
}
