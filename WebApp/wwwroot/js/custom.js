$(document).ready(function () {
    $(".tabContents").hide();
    $(".tabContents:first").show();
    $(".tabContaier ul li a").click(function () {
        var activeTab = $(this).attr("href");
        $(".tabContaier ul li a").removeClass("active");
        $(this).addClass("active");
        $(".tabContents").hide();
        $(activeTab).fadeIn();
        return false;
    });
});
$(function () {
    $('#da-slider').cslider({autoplay: true, bgincrement: 450});
});


$(document).ready(function () {
    $("#ajaxform").submit(function () {
        dataString = $("#ajaxform").serialize();
        $.ajax({
            type: "POST",
            url: "contact/send_mail",
            data: dataString,
            success: function (data) {
                $('#email-success').html("<h2><i class='fa fa-check-circle'></i> Contact Form Submitted, We will be in touch soon !</h2>").hide().fadeIn(2000);
            }
        });
        return false;
    });

});
