<div class="bottom-dev">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="bottom-list-item bottom-list-item-first">
                    <p><a href=""><i class="fa fa-cart-plus"></i></i></a></p>

                    <p>Products</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="bottom-list-item">
                    <p><a href=""><i class="fa fa-cloud-download"></i></a></p>

                    <p>Online Store</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="bottom-list-item">
                    <p><a href=""><i class="fa fa-camera-retro"></i></a></p>

                    <p>Photography</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="bottom-list-item">
                    <p><a href=""><i class="fa fa-coffee"></i></a></p>

                    <p>Entertainment</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="bottom-list-item">
                    <p><a href=""><i class="fa fa-align-center"></i></a></p>

                    <p>News</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="bottom-list-item">
                    <p><a href=""><i class="fa fa-graduation-cap"></i></a></p>

                    <p>Education</p>
                </div>
            </div>
        </div>
    </div>
</div>


<!--footer styles-->
<div class="t_footer">
    <div class="container">

        <div class="row footer_separet">
            <div class="col-sm-12">
                <h2 class="footer_heading">Fewlink IT Solution</h2>
            </div>
        </div>

        <div class="row segment-1 footer_separet">
            <div class="col-sm-2">
                <h2>About Fewlink</h2>
                <ul>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> Star Company Profile</a>
                    </li>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> The way we work</a></li>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> Mission</a></li>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> Vision</a></li>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> Innovation</a></li>
                </ul>
            </div>


            <div class="col-sm-2">
                <h2>Services</h2>
                <ul>
                    <li><a href="/services"><i class="fa fa-link"></i> Web Development</a></li>
                    <li><a href="/services"><i class="fa fa-link"></i> Software Development</a>
                    </li>
                    <li><a href="/services"><i class="fa fa-link"></i> Geo-location</a></li>
                    <li><a href="/services"><i class="fa fa-link"></i> IT Consulting</a></li>
                </ul>
            </div>


            <div class="col-sm-2">
                <h2>Products</h2>
                <ul>
                    <li><a href="/products/details/3/school-management-system-sms"><i
                                class="fa fa-link"></i> School Management</a></li>
                    <li><a href="/products/details/1/enterprise-resource-planning-erp-solution"><i
                                class="fa fa-link"></i> ERP Solution</a></li>
                    <li><a href="/products/details/6/online-accounting-system"><i
                                class="fa fa-link"></i> Accounting System</a></li>
                    <li><a href="/products/details/4/point-of-sales-system-pos"><i
                                class="fa fa-link"></i> Point of Sales</a></li>
                    <li><a href="/products/details/2/human-resource-management-hrm-system"><i
                                class="fa fa-link"></i> HRM Solution</a></li>
                    <li><a href="/products/details/5/electronic-commerce-e-commerce"><i
                                class="fa fa-link"></i> E-commerce</a></li>
                    <li><a href="/products"><i class="fa fa-link"></i> CRM System</a></li>
                    <li><a href="/products"><i class="fa fa-link"></i> EIMS</a></li>
                </ul>
            </div>


            <div class="col-sm-2">
                <h2>Solutions</h2>
                <ul>

                    <li><a href=""><i class="fa fa-link"></i> IT Infrastructure</a></li>
                    <li><a href=""><i class="fa fa-link"></i> Back-Office Systemg</a></li>
                    <li><a href=""><i class="fa fa-link"></i> Education Publishing </a></li>
                    <li><a href=""><i class="fa fa-link"></i> Legal Services</a></li>
                </ul>
            </div>


            <div class="col-sm-2">
                <h2>Company</h2>
                <ul>
                    <li><a href="/aboutus"><i class="fa fa-link"></i> About Us</a></li>
                    <li><a href="/contact"><i class="fa fa-link"></i> Contact</a></li>
                    <li><a href="/career"><i class="fa fa-link"></i> Careers</a></li>
                    <li><a href="/login"><i class="fa fa-link"></i> Resources</a></li>
                    <li><a href="/login"><i class="fa fa-link"></i> Events</a></li>
                    <li><a href="/login"><i class="fa fa-link"></i> News</a></li>
                    <li><a href="/media"><i class="fa fa-link"></i> Media</a></li>
                </ul>
            </div>


            <div class="col-sm-2">
                <h2>Quick Link</h2>
                <ul>
                    <li><a href="/login"><i class="fa fa-link"></i> Login</a></li>
                    <li><a href="/registration"><i class="fa fa-link"></i> Registration</a></li>
                    <li><a href="/blog"><i class="fa fa-link"></i> Blog</a></li>
                    <li><a href="/"><i class="fa fa-link"></i> Forum</a></li>
                    <li><a href="/login"><i class="fa fa-link"></i> Fewlink Platform</a></li>

                </ul>
            </div>

        </div>

        <div class="row segment-2 footer_separet">
            <div class="col-sm-2"><img src="/images/site/logo.png" alt="fewlink"
                                       class="footer_logo"/></div>
            <div class="col-sm-6">
                <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor to
                    provide a complete IT management solution delivered both via cloud and on-premise. Fewlink
                    technology empowers MSPs and mid-sized enterprises to proactively manage and control their IT
                    environments remotely, easily and efficiently from a single platform. Fewlink solutions are in use
                    by more than 10,000 customers worldwide in a wide variety of industries, including retail,
                    manufacturing, healthcare, education, government, media, technology, finance, and more. Fewlink is
                    privately held with a presence in over 20 countries</p>
            </div>

            <div class="col-sm-1"></div>

            <div class="col-sm-3 bottom_contact">
                <h3>+88 01711-519594</h3>
                <h3>+88 01953-408553</h3>
                <h3>Skype : fewlink</h3>
                <h3>Email : info@fewlink.com</h3>

                <p>House # B28, Sector # 3 <br> Uttara, Dhaka-1230, Bangladesh</p>

                <p class="bottom-social-icon">
                    <a target="_blank" href="https://www.facebook.com/fewlink"><i class="fa fa-facebook"></i></a>
                    <a target="_blank" href="https://plus.google.com/b/106301683634504496142/+Fewlink/posts"><i class="fa fa-google-plus"></i></a>
                    <a target="_blank" href="https://twitter.com/Fewlink"><i class="fa fa-twitter"></i></a>
                    <a target="_blank" href="https://www.linkedin.com/company/fewlink-it-solution?trk=company_logo"><i class="fa fa-linkedin"></i></a>
                    <a target="_blank" href="https://www.youtube.com/channel/UCCpCsybKeT-x8ozlCx-rBMA"><i class="fa fa-youtube-play"></i></a>
                </p>
            </div>


        </div>


        <!--footer reserve-->

        <div class="row">
            <div class="col-md-12 footer-reserve">
                <p><a href="">Legal</a> &bull; <a href="">Terms</a> &bull; <a href="">Privacy</a> &bull; 2000-<?php echo date("Y");?> Fewlink IT Solution.</p>
                <p>Powered by Fewlink</p>
            </div>
        </div>

    </div>
</div>


<script src="/scripts/jquery-2.1.4.min.js"></script>
<script src="/scripts/bootstrap.min.js"></script>
<script src="/scripts/jquery.cslider.js"></script>
<script src="/scripts/modernizr_cslider.js"></script>
<script src="/scripts/custom.js"></script>


</body>
</html>






