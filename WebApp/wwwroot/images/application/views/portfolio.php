<div class="container container-lavel">
    <div class="row">
        <?php foreach ($portfolio as $portfolio) { ?>
            <div class="col-sm-3 view view-third">
                <div class="portfolio-border">
                    <div class="view-lavel">
                        <img src="/images/portfolio/<?php echo $portfolio->portfolio_image; ?>"
                             alt="<?php echo $portfolio->portfolio_title; ?>"/>

                        <div class="mask">
                            <h2><?php echo $portfolio->portfolio_title; ?></h2>

                            <div class="description"><p><?php echo $portfolio->portfolio_description; ?></p></div>
                            <p>
                                <a href="/portfolio/details/<?php echo $portfolio->portfolio_id; ?>"
                                   class="btn btn-danger"><i class="fa fa-link"></i> More Sample</a></p>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="pagination-box">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>