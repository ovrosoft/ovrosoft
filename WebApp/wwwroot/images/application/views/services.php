<!--Technolgy-->
<div class="container_separat">
    <div class="container technology_icon">

        <!---technology first step-->

        <div class="row technology_separet">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/erp_solution.jpg"
                                               alt="fewlink erp solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-bar-chart"></i> ERP Solution</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/erp"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-hrm.jpg"
                                               alt="fewlink hrm solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-briefcase"></i> HRM Solution</h2>

                        <p>Fewlink have policy of using Asp .Net for web developments
                            <a href="/hrm"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-export-import.jpg"
                                               alt="export import solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-arrows-alt"></i> EIMS Solution</h2>

                        <p>Fewlink have policy of using Asp .Net for web developments <br>
                            <a href="/eims" class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <!---technology second step-->

        <div class="row technology_separet">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink_andorid.jpg"
                                               alt="fewlink android solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-android"></i> Android Software</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink_windows_phone.png"
                                               alt="fewlink windows phone solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-windows"></i> Windows Phone Software</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution
                            <a href="/"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink_real_estate.jpg"
                                               alt="real estate solution"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-building-o"></i> Real Estate Solution</h2>

                        <p>We are used MS SQL Server 2008 for most secured data base for web and desktop application
                            <a href="/real_estate" class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <!---technology second step-->

        <div class="row technology_separet">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img
                            src="/images/services/fewlink-school-management-system.jpg"
                            alt="school management system"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-graduation-cap"></i> School Management</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/school_management"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-ecommerce.jpg"
                                               alt="fewlink ecommerce"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-shopping-cart"></i> E-Commerce</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/e_commerce"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-point-of-sales.jpg"
                                               alt="point of sales"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-tachometer"></i> Point of Sales</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/pos"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <!---technology third step-->

        <div class="row technology_separet">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/hr-payroll.jpg"
                                               alt="hr and payroll"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-usd"></i> SEO</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/seo_dev"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img
                            src="/images/services/fewlink-accounting-system.jpg"
                            alt="fewlink accounting system"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-university"></i> Financial Accounting</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/accounting"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-it-consulting.jpg"
                                               alt="fewlink it consulting"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-delicious"></i> IT Consulting</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/it_consulting" class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

        </div>

        <!---technology fourt step-->
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-it-course.jpg"
                                               alt="it course"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-university"></i> IT Course</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/it_training"
                               class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img src="/images/services/fewlink-it-development.jpg"
                                               alt="fewlink it developemnt"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-codepen"></i> Software Development</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/software_dev" class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-md-4"><img
                            src="/images/services/fewlink-social-marketing.jpg"
                            alt="ewlink-social-marketing"/></div>
                    <div class="col-md-8">
                        <h2><i class="fa fa-dribbble"></i> Social Marketing</h2>

                        <p>Fewlink is a leading global provider of IT systems management software. It is the only vendor
                            to provide a complete IT management solution.
                            <a href="/social_marketing" class="btn btn-sm pull-right">Details &raquo;</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



