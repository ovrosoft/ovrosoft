<div class="left-menu">
    <h2>Blog Categories</h2>
    <ul class="tree">
        <?php foreach($menu as $menu){?>
            <li><i class="fa fa-folder-open"></i> <?php echo $menu['type_name'];?></a>
                <ul>
                    <?php foreach($menu['type_id'] as $category){?>
                        <li><a href="/blog/category/<?php echo $category['cat_id'];?>/<?php echo url_link($category['cat_name']);?>"><i class="fa fa-file-text"></i> <?php echo $category['cat_name'];?></a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
    </ul>
</div>

<div class="blog-tab">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Popular
                Post</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Recent Post</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="home">
            <?php foreach ($popular as $popular) { ?>
                <div class="small-blog-box">
                    <a href="/blog/details/<?php echo $popular->blog_id; ?>"><img src="/images/blog/<?php echo $popular->blog_image; ?>" alt=""></a>
                    <h2><?php echo $popular->blog_title; ?></h2>
                    <ul>
                        <li><i class="fa fa-user"></i> <?php echo $popular->post_person; ?></li>
                        <li><i class="fa fa-comments"></i> 6 Comments</li><br>
                        <li><i class="fa fa-calendar"></i> <?php echo $popular->post_date; ?></li>
                    </ul>
                    <p><?php echo substr($popular->blog_note,0,110); ?> . . . .</p>
                </div>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="profile">
            <?php foreach ($others as $others) { ?>
                <div class="small-blog-box">
                    <a href="/blog/details/<?php echo $others->blog_id; ?>"><img src="/images/blog/<?php echo $others->blog_image; ?>" alt=""></a>
                    <h2><?php echo $others->blog_title; ?></h2>
                    <ul>
                        <li><i class="fa fa-user"></i> <?php echo $others->post_person; ?></li>
                        <li><i class="fa fa-comments"></i> 6 Comments</li><br>
                        <li><i class="fa fa-calendar"></i> <?php echo $others->post_date; ?></li>
                    </ul>
                    <p><?php echo substr($others->blog_note,0,110); ?> . . . .</p>
                </div>
            <?php } ?>
        </div>
    </div>

</div>
