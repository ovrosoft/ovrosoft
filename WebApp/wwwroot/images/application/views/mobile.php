<!--mobile application solution-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Mobile/Tab Order System</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-4"><img src="/images/products/apple.jpg" alt="accounting solution"/></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. Inventory reports tell you what products are running low, how much to order and when you need to re-order.</p>                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/eims" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

