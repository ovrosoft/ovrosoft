<div class="blog-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <?php foreach($blog as $blog){?>
                        <div class="col-sm-12">
                            <div class="blog-box">
                                <a href="/blog/details/<?php echo $blog->blog_id;?>/<?php url_link($blog->blog_title);?>"><img src="/images/blog/<?php echo $blog->blog_image;?>" alt=""></a>
                                <div class="blog-box-container">
                                <h1><?php echo $blog->blog_title;?></h1>
                                <ul>
                                    <li><i class="fa fa-user"></i> <?php echo $blog->post_person;?></li>
                                    <li><i class="fa fa-comments"></i> 6 Comments</li>
                                    <li><i class="fa fa-calendar"></i> <?php echo $blog->post_date;?></li>
                                </ul>
                                <p><?php echo $blog->blog_note;?></p>

                                <div class="blog-more">
                                    <div class="pull-left share-btn">
                                        <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                        <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                        <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                        <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="/blog/details/<?php echo $blog->blog_id;?>/<?php url_link($blog->blog_title);?>" class="btn btn-sm btn-primary">READ MORE</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pagination-box">
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <?php include('blog_menu.php');?>
            </div>
        </div>
    </div>
</div>