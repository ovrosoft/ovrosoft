<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/site/fewlink.ico">
    <link rel="stylesheet" href="/styles/bootstrap.min.css">
    <link rel="stylesheet" href="/styles/font-awesome.min.css">
    <link rel="stylesheet" href="/styles/main.css">
    <link rel="stylesheet" href="/styles/parallax-slider.css">
    <script src="/scripts/api_script.js"></script>
    <meta name="title" content="<?php echo $meta->meta_title; ?>">
    <meta name="description" content="<?php echo $meta->meta_description; ?>">
    <meta name="keywords" content="<?php echo $meta->meta_keywords; ?>">
    <meta property="og:site_name" content="Fewlink IT Solution">
    <meta property="og:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
    <meta property="og:title" content="<?php echo $meta->meta_title; ?>">
    <meta property="og:description" content="<?php echo $meta->meta_description; ?>">
    <meta property="al:ios:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
    <meta property="al:android:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
    <meta property="al:web:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
    <meta name="twitter:site" content="@fewlink">
    <meta name="twitter:url" content="<?php echo base_url() . $this->uri->uri_string(); ?>">
    <meta name="twitter:title" content="<?php echo $meta->meta_title; ?>">
    <meta name="twitter:description" content="<?php echo $meta->meta_description; ?>">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="en">
    <meta name='robots' content='all'/>
    <title><?php echo $meta->meta_title; ?></title>
    <meta name="author" content="Fewlink IT Solution"/>
</head>
<body>

<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <ul class="top_bar_list">
                    <li><a href="/technology" <?php if ($this->uri->segment(1) == "technology") {
                            echo 'class="select"';
                        } ?>><i class="fa fa-book"></i> Technology</a></li>

                    <li><a href="/portfolio" <?php if ($this->uri->segment(1) == "portfolio") {
                            echo 'class="select"';
                        } ?>><i class="fa fa-picture-o"></i> Portfolio</a></li>

                    <li><a href="/company" <?php if ($this->uri->segment(1) == "company") {
                            echo 'class="select"';
                        } ?>><i class="fa fa-picture-o"></i> Company</a></li>

                    <li><a href="/aboutus" <?php if ($this->uri->segment(1) == "aboutus") {
                            echo 'class="select"';
                        } ?>><i class="fa fa-external-link"></i> About Us</a></li>
                    <!--
                    <li><a href="/news" <?php if ($this->uri->segment(1) == "news") {
                            echo 'class="select"';
                        } ?>><i class="icon fa fa-user"></i> News</a></li>
                    -->
                    <li><a href="/blog" <?php if ($this->uri->segment(1) == "blog") {
                            echo 'class="select"';
                        } ?>><i class="icon fa fa-user"></i> Blog</a></li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul class="top_bar_right header_mobile">
                    <li><i class="fa fa-phone"></i> 01953-408553</li>
                    <li><i class="fa fa-phone"></i> 01711-519594</li>
                    <li><i class="fa fa-skype"></i> fewlink</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-6"><a href="/"><img src="/images/site/logo.png" alt="Fewlink Logo"/></a></div>
            <div class="col-sm-6">
                <ul class="social_icon">
                    <li><a target="_blank" href="https://www.facebook.com/fewlink"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li><a target="_blank" href="https://www.linkedin.com/company/fewlink-it-solution?trk=company_logo"><i
                                class="fa fa-linkedin"></i></a></li>
                    <li><a target="_blank" href="https://twitter.com/Fewlink"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="https://plus.google.com/b/106301683634504496142/+Fewlink/posts"><i
                                class="fa fa-google-plus"></i></a></li>
                    <li><a target="_blank" href="https://www.youtube.com/channel/UCCpCsybKeT-x8ozlCx-rBMA"><i
                                class="fa fa-youtube-play"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="top_menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="top_menu_list">
                    <li><a href="/" <?php if ($this->uri->segment(1) == "") {
                            echo 'class="select"';
                        } ?>>Home</a></li>
                    <li><a href="/software" <?php if ($this->uri->segment(1) == "software") {
                            echo 'class="select"';
                        } ?>>Software</a></li>
                    <li><a href="/website" <?php if ($this->uri->segment(1) == "website") {
                            echo 'class="select"';
                        } ?>>Web Site</a></li>
                    <li><a href="/mobile" <?php if ($this->uri->segment(1) == "mobile") {
                            echo 'class="select"';
                        } ?>>Mobile Apps</a></li>
                    <li><a href="/services" <?php if ($this->uri->segment(1) == "services") {
                            echo 'class="select"';
                        } ?>>Services</a></li>
                    <li><a href="/products" <?php if ($this->uri->segment(1) == "products") {
                            echo 'class="select"';
                        } ?>>Products</a></li>
                    <li><a href="/contact" <?php if ($this->uri->segment(1) == "contact") {
                            echo 'class="select"';
                        } ?>>Contact US</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    function social(){
        window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;
    }
</script>
