<!--erp solution-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Enterprise Resource Planning (ERP) Solution</h2>
            <p>Customize Solution</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-4"><img src="/images/products/apple.jpg" alt="erp solution"/></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Financial management</li>
                                <li>Human Resource Management</li>
                                <li>Customer Relationship</li>
                                <li>Inventory management</li>
                                <li>Sales and order entry</li>
                                <li>Procurement</li>
                                <li>Dashboards</li>
                                <li>Technological fit</li>
                                <li>Emailing</li>
                                <li>Reporting</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink ERP software solutions provide for small and mid-market manufacturing and distribution companies. Our ERP exclusive focus is on the development, sale, implementation, and support of Enterprise ERP, the companies fully-integrated ERP software system. From superior software functionality and a proven ERP implementation methodology, to one year of free maintenance and industry-leading direct developer customer support, Fewlink ERP software and service offerings that are truly unique in the ERP software industry.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/erp" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!--online accounting-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Online Accounting System</h2>
            <p>Easy Accounting for your Small Business.</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Cash Management</li>
                                <li>Banking Transaction</li>
                                <li>Voucher List</li>
                                <li>Accounts Receiveable</li>
                                <li>Accounts Payable</li>
                                <li>Journal </li>
                                <li>Ledger</li>
                                <li>Trail Balance</li>
                                <li>Income Statement</li>
                                <li>Balance Sheet </li>
                                <li>Asset Management</li>
                                <li>Depriciation Management</li>
                                <li>Budget Information</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink Accounting System is an online accounting system that aims to automate businesses, to make financial management easier and more efficient.Fewlink was originally conceived as a stand-alone web-based accounting system that can be deployed for small businesses. Fewlink was built to give MSMEs access to an affordable and reliable cloud accounting system that will help them manage their businesses.Using the double entry accounting system as its core framework.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/accounting" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"><img src="/images/products/erp_light.png" alt="accounting solution"/></div>
            </div>

        </div>
    </div>
</div>


<!--real estate-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Real Estate Cost Managment Solution</h2>
            <p>Project Cost Minimize Solution.</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-4"><img src="/images/products/product-light.png" alt="accounting solution"/></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Cash Management</li>
                                <li>Banking Transaction</li>
                                <li>Voucher List</li>
                                <li>Accounts Receiveable</li>
                                <li>Accounts Payable</li>
                                <li>Journal </li>
                                <li>Ledger</li>
                                <li>Trail Balance</li>
                                <li>Income Statement</li>
                                <li>Balance Sheet </li>
                                <li>Asset Management</li>
                                <li>Depriciation Management</li>
                                <li>Budget Information</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink Accounting System is an online accounting system that aims to automate businesses, to make financial management easier and more efficient.Fewlink was originally conceived as a stand-alone web-based accounting system that can be deployed for small businesses. But seeing that 99% of the businesses are Micro-Small-Medium-Enterprises (MSME) who can't afford popular business software, Fewlink was built to give MSMEs access to an affordable and reliable cloud accounting system that will help them manage their businesses.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/real_estate" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--real estate-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>School Management System (SMS)</h2>
            <p>Multipurpose Education System</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Academic Managment</li>
                                <li>Student Information</li>
                                <li>Parents</li>
                                <li>HRM</li>
                                <li>Admission</li>
                                <li>Report</li>
                                <li>Alumni</li>
                                <li>Examination</li>
                                <li>Results</li>
                                <li>Accounts</li>
                                <li>Staff Information</li>
                                <li>Transport</li>
                                <li>Progress </li>
                                <li>Banking</li>
                                <li>Library</li>
                                <li>SMS Notification</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Our School Management System(SMS) is a revolutionary school management software introduced to simplify the complexities faced in daily school operations. Use Fewlink SMS  to effectively manage processes and operations related to teachers, students, courses, employees and soon. Our School Management System is a customizable software that suits for a variety of educational institutions including.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/school_management" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"><img src="/images/products/apples.jpg" alt="accounting solution"/></div>
            </div>
        </div>
    </div>
</div>

<!--hrm solution-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Human Resource Management (HRM) System</h2>
            <p>Web Based Complete HR System</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-4"><img src="/images/products/apples.jpg" alt="accounting solution"/></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>User</li>
                                <li>Configuration</li>
                                <li>Employee Information</li>
                                <li>Loan</li>
                                <li>Recruitment</li>
                                <li>Termination</li>
                                <li>Training</li>
                                <li>Interview</li>
                                <li>Resign</li>
                                <li>Attendence</li>
                                <li>Salary</li>
                                <li>Roster</li>
                                <li>Leave</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink HRM software provides organisations with the tools and functionality required to manage the employment of personnel using the most cost effective, process efficient and compliant methods.We provide a one-stop solution to comprehensively meet customer needs, from system planning and development to infrastructure deployment, and system operation and maintenance. These include not only solutions that can speed up operations and increase efficiency but also ways to strengthen personnel management compliance.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/hrm" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--pos solution-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Point of Sales System (POS)</h2>
            <p>Checking out should be simple</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains.</p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/pos" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4"><img src="/images/products/apples.jpg" alt="accounting solution"/></div>
            </div>
        </div>
    </div>
</div>


<!--pos solution-->
<div class="container products_pack">
    <div class="row">
        <div class="col-sm-12 products-head">
            <h2>Export Import Management(EIMS)</h2>
            <p>Import Export Easy Solution</p>
        </div>
        <div class="col-sm-12">
            <div class="row product-box">
                <div class="col-sm-4"><img src="/images/products/apple.jpg" alt="accounting solution"/></div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12 feature-list">
                            <ul>
                                <li>Configuration</li>
                                <li>Purchase </li>
                                <li>Sales</li>
                                <li>Stock Management</li>
                                <li>Accounts</li>
                                <li>Supplier </li>
                                <li>Customer</li>
                                <li>Items</li>
                                <li>Product Return</li>
                                <li>Branch Stock</li>
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            <p>Fewlink Point of Sale is a POS software application for managing retail sales transactions and product inventory control. It runs on any ordinary PC with a receipt printer. A keyboard intercept (or keyboard emulation) bar code scanner, cash tray, display and credit card merchant account are optional.If your businesses sells goods, services or a combination of the two, you can use Fewlink Point of Sale(POS) to easily manage your retail sales transactions. The optional server application can share data between different retail store chains. </p>
                            <div class="product-more">
                                <div class="pull-left share-btn">
                                    <a class="facebook" href="#"><i class="fa fa-facebook"></i> Share</a>
                                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i> Share</a>
                                    <a class="twitter" href="#"><i class="fa fa-twitter"></i> Tweet</a>
                                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/eims" class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>