<div class="news-tab">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Recent News</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Popular News</a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="home">
            <?php for($i=0;$i<10;$i++) { ?>
                <div class="news-slide">
                    <div class="slide_note">
                        <img src="/images/news/news2.jpg" alt="">

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. ...</p>
                    </div>
                    <ul>
                        <li><i class="fa fa-comments"></i> 6 Comments</li>
                        <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                    </ul>
                </div>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="profile">
            <?php for($i=0;$i<10;$i++) { ?>
                <div class="news-slide">
                    <div class="slide_note">
                        <img src="/images/news/news2.jpg" alt="">

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. ...</p>
                    </div>
                    <ul>
                        <li><i class="fa fa-comments"></i> 6 Comments</li>
                        <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
