<div class="container">
    <div class="row">
        <div class="contact-page">
            <div class="col-md-12 contact-map">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3648.724025926994!2d90.39658754233001!3d23.863931636666887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c423d9e8629b%3A0xb812987fe32d9199!2sSector+3%2C+Dhaka+1230!5e0!3m2!1sbn!2sbd!4v1435653115912"
                    width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-md-9 contact-form">
                <div class="row">
                    <form name="ajaxform" id="ajaxform" action="" method="get" class="register-form">
                        <div class="col-md-12 contact-title">
                            <h4>Contact Form</h4>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label class="info-title">Your Name <span>*</span></label>
                                <input name="txt_name" type="text" class="form-control unicase-form-control text-input"
                                       placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="info-title"">Email Address
                                <span>*</span></label>
                                <input name="txt_email" type="email"
                                       class="form-control unicase-form-control text-input"
                                       placeholder="example@example.com" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="info-title">Mobile Number
                                    <span>*</span></label>
                                <input name="txt_mobile" type="text"
                                       class="form-control unicase-form-control text-input"
                                       placeholder="01711-******" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="info-title">Your Message
                                    <span>*</span></label>
                                <textarea name="txt_message" class="form-control unicase-form-control"
                                          id="exampleInputComments"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 outer-bottom-small m-t-20">
                            <input type="submit" name="button" id="button"
                                   class="btn btn-primary" value="Send Message">
                        </div>
                        <div class="col-md-9 outer-bottom-small m-t-20">
                            <div id="email-success"></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-3 contact-info">
                <div class="contact-title">
                    <h4>INFORMATION</h4>
                </div>
                <div class="clearfix address">
                    <span class="contact-i"><i class="fa fa-map-marker"></i></span>
                    <span class="contact-span">House # B28, Sector # 3, Uttara <br> Dhaka-1230, Bangladesh</span>
                </div>
                <div class="clearfix phone-no">
                    <span class="contact-i"><i class="fa fa-mobile"></i></span>
                    <span class="contact-span">+88 01711-51 95 94<br>+88 01953-40 85 53</span>
                </div>
                <div class="clearfix skype">
                    <span class="contact-i"><i class="fa fa-skype"></i></span>
                    <div class="contact-span contact-skype">fewlink</div>
                </div>
                <div class="clearfix email">
                    <span class="contact-i"><i class="fa fa-envelope"></i></span>
                    <span class="contact-span">info@fewlink.com <br>sales@fewlink.com</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div class="email-success">
    <p>Your Message Submitted!</p>

    <p> We will be in touch soon</p>
</div>
-->