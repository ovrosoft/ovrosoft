<div class="news-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="latest-news-box">
                    <h1>Latest News</h1>
                    <div class="main-news">
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="last-news-box">
                                    <img src="/images/news/news1.jpg" alt="">

                                    <h2>13,000+ People Have Bought Our Theme</h2>
                                    <ul>
                                        <li><i class="fa fa-comments"></i> 6 Comments</li>
                                        <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias aliquam aliquid amet
                                        atque commodi dolorum, eos eveniet excepturi expedita hic illo ipsam magni odit possimus
                                        quasi sunt temporibus vel, voluptatibus magni odit possimus quasi sunt temporibus vel, voluptatibus?</p>

                                    <p><a href="" class="btn btn-primary btn-sm">Read More</a></p>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="row">
                                    <?php for ($i = 0; $i < 5; $i++) { ?>
                                        <div class="col-sm-12">
                                            <div class="news-slide">
                                                <div class="slide_note">
                                                    <img src="/images/news/news2.jpg" alt="">

                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. ...</p>
                                                </div>
                                                <ul>
                                                    <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                    <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-news">
                    <h1>Latest News</h1>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="carousel-news-box">
                                            <img src="/images/news/news2.jpg" alt="">
                                            <h4>Lorem ipsum dolor sit amet</h4>
                                            <ul>
                                                <li><i class="fa fa-comments"></i> 6 Comments</li>
                                                <li><i class="fa fa-calendar"></i> 2015-10-02 13:06:49</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>



            </div>
            <div class="col-sm-4">
                <?php include('news_category.php'); ?>
            </div>
        </div>
    </div>
</div>
