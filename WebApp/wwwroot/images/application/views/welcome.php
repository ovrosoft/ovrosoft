
<div class="default_message">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Welcome to Fewlink IT Solution</h1>

                <p>Fewlink is your All IT Support Company</p>

                <p class="company-text">We are provide all kinds of customize Software <br> Website, Android Application
                    and Windows Phone Application</p>
            </div>
        </div>
    </div>
</div>


<div class="container_separat">
    <div class="container">
        <!--services first step-->
        <div class="row services">

            <div class="col-sm-6">
                <h2><i class="fa fa-newspaper-o"></i> Web Application Development</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-web-application.jpg"
                                               alt="fewlink-web-application"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/web_app"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <h2><i class="fa fa-desktop"></i> Software Development</h2>

                <div class="row">
                    <div class="col-sm-4"><img
                            src="/images/welcome/fewlink-software-development.jpg"
                            alt="fewlink software development"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/software_dev"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

        </div>


        <!--services second step-->
        <div class="row services">

            <div class="col-sm-6">
                <h2><i class="fa fa-paw"></i> Mobile Application Solution</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-web-design.jpg"
                                               alt="fewlink web design"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br> <a href="/mobile_app"
                                                                        class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <h2><i class="fa fa-shopping-cart"></i> E-commerce Solution</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-e-commerce.jpg"
                                               alt="fewlink e commerce"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/e_commerce"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

        </div>


        <!--services third step-->
        <div class="row services">
            <div class="col-sm-6">
                <h2><i class="fa fa-recycle"></i> IT Training</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-it-training.jpg"
                                               alt="fewlink it training"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/it_training"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <h2><i class="fa fa-search"></i> Search engine optimization (SEO)</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-seo.jpg"
                                               alt="fewlink seo"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/seo_dev"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

        </div>


        <!--services fourth step-->
        <div class="row services">
            <div class="col-sm-6">
                <h2><i class="fa fa-life-ring"></i> Outsourcing Solution</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-out-sourcing.jpg"
                                               alt="fewlink out sourcing"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/outsourcing"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <h2><i class="fa fa-phone-square"></i> IT Consulting</h2>

                <div class="row">
                    <div class="col-sm-4"><img src="/images/welcome/fewlink-it-consulting.jpg"
                                               alt="ewlink it consulting"/></div>
                    <div class="col-sm-8">Fewlink is a leading global provider of IT systems management software. It is
                        the only vendor to provide a complete IT management solution delivered both via cloud and
                        on-premise. Fewlink technology empowers MSPs and mid-sized enterprises to proactively manage and
                        control their IT environments remotely. <br><a href="/it_consulting"
                                                                       class="btn btn-sm pull-right">Details &raquo;</a>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>


<!--Welcome message-->
<div class="container">
    <div class="row welcome_message">
        <h1>Our Ready Software</h1>

        <p class="marketing-byline">Fewlink IT Solution</p>
    </div>
</div>


<!--Work history-->
<div class="container_separat">
    <div class="container">
        <div class="row work_history">
            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/welcome/accounting.jpg" alt=""/>

                    <h2>Accounting Software</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/accounting"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office2.jpg" alt=""/>

                    <h2>School Management</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/school_management"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office3.jpg" alt=""/>

                    <h2>ERP Solution</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/erp"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office4.jpg" alt=""/>

                    <h2>HRM Solution</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/hrm"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>


            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office1.jpg" alt=""/>

                    <h2>E-Commerce Solution</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/e_commerce"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office2.jpg" alt=""/>

                    <h2>Point of Sales</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/pos"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office3.jpg" alt=""/>

                    <h2>Export Import Management</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/eims"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="content-border content_padding">
                    <img src="/images/work/bootstrap-templates-office4.jpg" alt=""/>

                    <h2>Real Estate Solution</h2>

                    <p>Fewlink is a leading global provider of IT Company.It is the only vendor provide a complete IT
                        Solution</p>

                    <p class="pull-center"><a
                            href="/real_estate"
                            class="btn btn-primary btn-sm"> <span class="glyphicon glyphicon-link"></span> Details</a>
                    </p>
                </div>
            </div>


        </div>
    </div>
</div>
