
<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-8">
            <?php foreach ($single as $single) { ?>
                <div class="portfolio-pan">
                    <img
                        src="/images/slide/<?php echo $single->image_path; ?>/<?php echo $single->slide_image; ?>"
                        alt=""/>

                    <h2><i class="fa fa-link"></i> <?php echo $single->slide_title; ?></h2>

                    <p class="note"><?php echo $single->slide_note; ?></p>

                    <p><a href="<?php echo $single->site_url; ?>" class="btn btn-sm btn-primary" target="_blank"><i
                                class="fa fa-link"></i> Visit Site</a></p>

                    <p style="float:left">
                        <span class="date"><i class="fa fa-calendar"></i> 25 Jan, 2015</span>

                    <div class="fb-like btn-share" data-href="<?php echo $single->fb_url; ?>" data-layout="standard"
                         data-action="like" data-show-faces="true" data-share="true"></div>
                    </p>

                </div>
            <?php } ?>
        </div>

        <div class="col-sm-4">

        </div>
    </div>
</div>