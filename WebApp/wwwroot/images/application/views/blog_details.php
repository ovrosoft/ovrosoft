<div class="blog-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                        <div class="col-sm-12">
                            <div class="blog-box">
                                <a href="/blog/details/<?php echo $single->blog_id;?>"><img src="/images/blog/<?php echo $single->blog_image;?>" alt=""></a>
                                <div class="blog-box-container">
                                <h1><?php echo $single->blog_title;?></h1>
                                <ul>
                                    <li><i class="fa fa-user"></i> <?php echo $single->post_person;?></li>
                                    <li><i class="fa fa-comments"></i> 6 Comments</li>
                                    <li><i class="fa fa-calendar"></i> <?php echo $single->post_date;?></li>
                                </ul>
                                <p><?php echo $single->blog_note;?></p>
                                <div class="blog-details"><?php echo $single->blog_details;?></div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-sm-3">
                <?php include('blog_menu.php');?>
            </div>
        </div>
    </div>
</div>
