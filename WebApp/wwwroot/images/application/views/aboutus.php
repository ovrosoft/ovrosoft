<div class="about-focus-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="focus-box focus-perfection">
                    <p class="focus-icon"><i class="fa fa-thumbs-o-up"></i></p>
                    <h3>PERFECTION</h3>
                    <img src="/images/aboutus/handshake.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="focus-box focus-in-time">
                    <p class="focus-icon"><i class="fa fa-calendar-check-o"></i></p>
                    <h3>IN TIME</h3>
                    <img src="/images/aboutus/watch.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="focus-box focus-no-stress">
                    <p class="focus-icon"><i class="fa fa-smile-o"></i></p>
                    <h3>NO STRESS</h3>
                    <img src="/images/aboutus/no-strees.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="focus-box focus-finance">
                    <p class="focus-icon"><i class="fa fa-bar-chart"></i></p>
                    <h3>FINANCE</h3>
                    <img src="/images/aboutus/finance.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="choose-us-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="choose-title">
                <h2>WHY CHOOSE FEWLINK</h2>
                <p>ONE MINIFOX IS BETTER THAN TWO OF SOMETHING ELSE.<br> GET YOUR DAILY DOSE OF MINIFOX</p>
                    <hr><hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="choose-box text-right">
                    <h4><span>CREATIVE DESIGNERS</span> <i class="fa fa-cloud"></i></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>

                <div class="choose-box text-right">
                    <h4><span>CREATIVE DESIGNERS</span> <i class="fa fa-cloud"></i></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>

                <div class="choose-box text-right">
                    <h4><span>CREATIVE DESIGNERS</span> <i class="fa fa-cloud"></i></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-center hidden-xs">
                <img src="/images/aboutus/nexus.png" alt="" class="center-block img-responsive">
                </div>
            </div>
            <div class="col-md-3">
                <div class="choose-box choose-box-right  text-left">
                    <h4><i class="fa fa-cloud"></i> <span>CREATIVE DESIGNERS</span></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>

                <div class="choose-box choose-box-right text-left">
                    <h4><i class="fa fa-cloud"></i> <span>CREATIVE DESIGNERS</span></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>

                <div class="choose-box choose-box-right text-left">
                    <h4><i class="fa fa-cloud"></i> <span>CREATIVE DESIGNERS</span></h4>
                    <p>Looking for best web designers on ! <br> Mass Themes are a full service digital agency so get ready to be impressed</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="let-us-help-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="let-us-help-title">
                <p>LET US HELP YOU</p>
                <h2>TELL THE WORLD ABOUT YOURSELF</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="let-us-help-box">
            <h3><i class="fa fa-headphones"></i> CUSTOMER SUPPORT</h3>
                <p>Our professional customer support team will make sure you get a great user experience with the theme with their fast and helpful assistance.</p>
            </div>
            </div>
            <div class="col-sm-4">
                <div class="let-us-help-box">
                <h3><i class="fa fa-rocket"></i> EASY SETUP</h3>
                <p>Intuitive admin panel, descriptive documentation and helpful support guarantee fast and easy website setup for your brand new online presence.</p>
            </div>
                </div>
            <div class="col-sm-4">
                <div class="let-us-help-box">
                <h3><i class="fa fa-shopping-cart"></i> FULL SHOP FUNCTIONALITY</h3>
                <p>The theme features WooCommerce support and has all the needed styles for it. You can setup a shop to sell your products directly from your website.</p>
            </div>
                </div>
            <div class="col-sm-4">
                <div class="let-us-help-box">
                <h3><i class="fa fa-magic"></i> UNLIMITED COLORS</h3>
                <p>All theme colors are managed with rally flexible color schemes, most colors can be changed globally and for separate elements too!</p>
            </div>
                </div>
            <div class="col-sm-4">
                <div class="let-us-help-box">
                <h3><i class="fa fa-th"></i> MEGA-MENU</h3>
                <p>Powerful custom Mega Menu plugin lets you create fantastic menus with flexible color and layout options, custom icons, titles and labels.</p>
            </div>
                </div>
            <div class="col-sm-4">
                <div class="let-us-help-box">
                <h3><i class="fa fa-cloud"></i> COOL PORTFOLIO LAYOUTS</h3>
                <p>Grid and Puzzle, with flexible appearance settings, your portfolio page will have a classy and unique look to attract attention to your projects.</p>
            </div>
                </div>
            </div>
        </div>
    </div>
</div>