<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Software_dev extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('software_dev');
        $this->load->view('header', $header);
        $this->load->view('software_dev');
        $this->load->view('footer');
    }
}
