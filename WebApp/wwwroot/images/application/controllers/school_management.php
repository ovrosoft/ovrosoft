<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class School_management extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('school_management');
        $this->load->view('header', $header);
        $this->load->view('school_management');
        $this->load->view('footer');
    }

}
