<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
        $this->load->model('blog_model');
    }

    public function index()
    {
        $config["base_url"] = base_url('blog/index');
        $config["total_rows"] = $this->blog_model->blog_count();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["pagination"] = $this->pagination->create_links();
        $header['meta'] = $this->custom_model->meta_data('blog');
        $data['blog'] = $this->blog_model->blog_list($config["per_page"], $page);
        $data['popular'] = $this->blog_model->popular_post();
        $data['others'] = $this->blog_model->others_post();
        $data['menu'] = $this->blog_model->blog_menu();
        $this->load->view('header', $header);
        $this->load->view('blog', $data);
        $this->load->view('footer');
    }

    public function details($blog_id)
    {
        $data['single'] = $this->blog_model->single_blog($blog_id);
        $header['meta'] = $this->blog_model->single_blog($blog_id);
        $data['popular'] = $this->blog_model->popular_post();
        $data['others'] = $this->blog_model->others_post();
        $data['menu'] = $this->blog_model->blog_menu();
        $this->load->view('header', $header);
        $this->load->view('blog_details', $data);
        $this->load->view('footer');
    }

    public function category($cat_id)
    {
        $config["base_url"] = base_url('blog/category/'.$cat_id);
        $config["total_rows"] = $this->blog_model->blog_category_count($cat_id);
        $config["per_page"] = 4;
        $config["uri_segment"] = 4;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["pagination"] = $this->pagination->create_links();

        $header['meta'] = $this->custom_model->meta_data($this->custom_model->blog_cat_meta($cat_id));
        $data['popular'] = $this->blog_model->popular_post();
        $data['others'] = $this->blog_model->others_post();
        $data['menu'] = $this->blog_model->blog_menu();
        $data['blog'] = $this->blog_model->blog_category($cat_id, $config["per_page"], $page);
        $this->load->view('header', $header);
        $this->load->view('blog_category', $data);
        $this->load->view('footer');
    }
}

