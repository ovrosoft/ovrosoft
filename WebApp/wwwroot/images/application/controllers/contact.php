<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Contact extends CI_Controller

{



    function __construct()

    {

        parent::__construct();

        $this->load->model('custom_model');

    }



    public function index()

    {

        $header['meta'] = $this->custom_model->meta_data('contact');

        $this->load->view('header', $header);

        $this->load->view('contact');

        $this->load->view('footer');

    }



    public function send_mail()

    {

        $mail_data="\n Sender Name : ".$this->input->post('txt_name')."\n Sender Mobile : ".$this->input->post('txt_mobile')."\n Sender Email : ".$this->input->post('txt_email')."\n Sender Message : ".$this->input->post('txt_message');

        $this->email->from($this->input->post('txt_email'), "Fewlink Client Mail");

        $this->email->to("khalifa.shahin@gmail.com");

        $this->email->cc("info@fewlink.com");

        $this->email->bcc("fewlinkit@gmail.com");

        $this->email->subject("This Email Sent from Customer");

        $this->email->message($mail_data);

        $this->email->send();

    }



}



