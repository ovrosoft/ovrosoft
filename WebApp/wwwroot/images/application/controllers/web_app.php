<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Web_app extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('web_app');
        $this->load->view('header', $header);
        $this->load->view('web_app');
        $this->load->view('footer');
    }
}
