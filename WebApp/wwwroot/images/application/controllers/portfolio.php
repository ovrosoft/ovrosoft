<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
        $this->load->model('portfolio_model');
    }

    public function index()
    {
        $config["base_url"] = base_url('portfolio/index');
        $config["total_rows"] = $this->portfolio_model->portfolio_count();
        $config["per_page"] = 12;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["pagination"] = $this->pagination->create_links();

        $header['meta'] = $this->custom_model->meta_data('portfolio');
        $data['portfolio'] = $this->portfolio_model->portfolio_list($config["per_page"], $page);
        $this->load->view('header', $header);
        $this->load->view('portfolio', $data);
        $this->load->view('footer');
    }

    public function details($portfolio_id)
    {
        $data['single'] = $this->portfolio_model->single_portfolio($portfolio_id);
        $header['meta'] = $this->custom_model->meta_data($this->custom_model->portfolio_meta($portfolio_id));
        $this->load->view('header', $header);
        $this->load->view('portfolio_details',$data);
        $this->load->view('footer');
    }

}

