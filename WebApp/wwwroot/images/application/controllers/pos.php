<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('pos');
        $this->load->view('header', $header);
        $this->load->view('pos');
        $this->load->view('footer');
    }
}
