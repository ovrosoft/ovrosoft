<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Real_estate extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('real_estate');
        $this->load->view('header', $header);
        $this->load->view('real_estate');
        $this->load->view('footer');
    }

}
