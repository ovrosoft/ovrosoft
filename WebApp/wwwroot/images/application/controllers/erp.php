<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Erp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_model');
    }

    public function index()
    {
        $header['meta'] = $this->custom_model->meta_data('erp');
        $this->load->view('header', $header);
        $this->load->view('erp');
        $this->load->view('footer');
    }
}
