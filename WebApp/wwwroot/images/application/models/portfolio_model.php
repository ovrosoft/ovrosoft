<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio_model extends CI_Model
{

    public function portfolio_count()
    {
        return $this->db->count_all('portfolio_list');
    }

    public function portfolio_list($limit, $start)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get('portfolio_list');
        return $query->result();
    }

    public function single_portfolio($portfolio_id)
    {
        $this->db->where('portfolio_id', $portfolio_id);
        $query = $this->db->get('portfolio_slide');
        return $query->result();
    }

}

