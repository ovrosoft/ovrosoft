<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends CI_Model
{

    public function blog_list($limit, $start)
    {
        $this->db->order_by('blog_id', 'desc');
        $this->db->limit($limit, $start);
        $query = $this->db->get('blog_list');
        return $query->result();
    }

    public function popular_post()
    {
        $this->db->limit(7);
        $query = $this->db->get('blog_list');
        return $query->result();
    }

    public function others_post()
    {
        $this->db->limit(7);
        $query = $this->db->get('blog_list');
        return $query->result();
    }

    public function blog_count()
    {
        return $this->db->count_all('blog_list');
    }

    public function blog_category_count($cat_id)
    {
        $this->db->where('cat_id', $cat_id);
        $query = $this->db->get('blog_list');
        return $query->num_rows();
    }


    public function single_blog($blog_id)
    {
        $this->db->where('blog_id', $blog_id);
        $query = $this->db->get('blog_list');
        return $query->row();
    }

    public function blog_menu()
    {
        $query = $this->db->get('blog_type');
        $result = $query->result_array();
        foreach ($result as $key => $row) {
            $query = $this->db->get_where('blog_category', array('type_id' => $row['type_id']));
            $row['type_id'] = $query->result_array();
            $result[$key] = $row;
        }
        return $result;
    }

    public function blog_category($cat_id,$limit, $start)
    {
        $this->db->where('cat_id', $cat_id);
        $this->db->limit($limit, $start);
        $this->db->order_by('blog_id', 'desc');
        $query = $this->db->get('blog_list');
        return $query->result();
    }

}

