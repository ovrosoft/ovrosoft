<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Custom_model extends CI_Model
{

    public function meta_data($meta_clip)
    {
        $this->db->where('meta_clip', $meta_clip);
        $query = $this->db->get('meta_data');
        return $query->row();
    }

    public function portfolio_meta($portfolio_id)
    {
        $this->db->where('portfolio_id', $portfolio_id);
        $query = $this->db->get('portfolio_list');
        return $query->row()->meta_clip;
    }

    public function blog_meta($blog_id)
    {
        $this->db->where('blog_id', $blog_id);
        $query = $this->db->get('blog_list');
        return $query->row()->meta_clip;
    }

    public function blog_cat_meta($cat_id)
    {
        $this->db->where('cat_id', $cat_id);
        $query = $this->db->get('blog_category');
        return $query->row()->meta_clip;
    }
}

